PHP-skills-test
==============

## Installing

* Fork this repository
* Execute `composer install` in cloned directory.

## Running the app

After you have installed all dependencies you can now run the app with:
```bash
php bin/console server:run
```

It will start a local server using `internal PHP web server`.
The port will be displayed to you as `http://localhost:8000`.

