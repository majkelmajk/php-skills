<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ExchangeController extends Controller
{
    /**
     * @Route("/exchange", name="exchange")
     */
    public function indexAction(Request $request)
    {

        $calculationForm = $request->get('calculationForm');

        if ($calculationForm['convert']) {

            /* Get the access to Exchange service instance */
            $exchange = $this->get('exchange.provider');

            /*
             * Parse the input string and get an array of all required exchange data, each containing:
             * base amount and input/output currencies
             */
            $conversions = $exchange->convertFromSentence( $calculationForm['convert'] );

            dump($conversions);


        }

        return $this->render('exchange/index.html.twig', [
	        'convert_sentence' => ( isset( $calculationForm['convert'] ) ? $calculationForm['convert'] : null ),
	        'known_currencies' => ( isset( $exchange ) ? $exchange->getKnownCurrencies() : [] ),
	        'conversions'      => ( isset( $conversions ) ? $conversions : [] )
        ]);

    }
}
