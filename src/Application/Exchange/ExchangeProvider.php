<?php
namespace Application\Exchange;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Application\Exception\CurrencyPairNotSupported;
use Money\Converter;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Exchange\FixedExchange;
use Money\Money;

class ExchangeProvider
{

	private $knownCurrencies = [
		'GBP' => ['symbol' => '£', 'name' => 'pound', 'name_plural_en' => 'pounds'],
		'EUR' => ['symbol' => '€', 'name' => 'euro',  'name_plural_en' => 'euros'],
		'PLN' => ['symbol' => 'zł', 'name' => 'złoty', 'name_plural_en' => 'złotys']
	];

	private $allowedCurrencyPairs = [
		'GBP/PLN',
		'GBP/EUR',
		'PLN/EUR',
		'PLN/GBP',
		'EUR/PLN'
	];

	private $exchangeRatesBuffer = [];

	private $container;

	public function __construct(Container $container) {
		$this->container = $container;
	}

    /**
     * Converts a single amount of money between currencies.
     *
     * @param int|float|string $base_amount
     * @param string $input_currency Input currency ISO code, e.g. GBP
     * @param string $output_currency Output currency ISO code, e.g. PLN
     * @return array containing: 'amount', 'rate', occasionally and 'error'.
     */
    public function convert($base_amount, $input_currency, $output_currency)
    {
    	$output = [];

        if ( ! $this->isCurrencyPairSupported( $input_currency, $output_currency) )
            return ['error' => 'Exchange rate "'. strtoupper($input_currency.'/'.$output_currency) .'" is not supported.'];

        $exchangeRate = $this->getExchangeRate($input_currency, $output_currency);

		$exchange = new FixedExchange([
			$input_currency => [
				$output_currency => $exchangeRate
			]
		]);

		$converter = new Converter( new ISOCurrencies(), $exchange );

	    $input_amount = new Money(($base_amount * 100), new Currency($input_currency));
	    $output_amount = $converter->convert($input_amount, new Currency($output_currency))->getAmount();
	    $output_amount = $output_amount / 100;

	    return ['amount'=>$output_amount, 'rate'=>$exchangeRate];
    }

	/**
	 * @param $sentence
	 *
	 * @return array
	 */
    public function convertFromSentence($sentence)
    {
        $conversionData = $this->parseSentence($sentence);

        foreach ($conversionData as $key=>$conversionElements) {
            $conversionData[$key]['converted'] = $this->convert( $conversionElements['base_amount'], $conversionElements['input_currency'], $conversionElements['output_currency'] );
        }

        return $conversionData;
    }

    public function getExchangeRate($input_currency, $output_currency)
    {
        if ( ! isset($this->exchangeRatesBuffer[$input_currency]) )
            $this->exchangeRatesBuffer[$input_currency] = [];

        if ( ! isset($this->exchangeRatesBuffer[$input_currency][$output_currency]) ) {
            $this->exchangeRatesBuffer[$input_currency][$output_currency] = $this->fetchExchangeRate($input_currency, $output_currency);
        }

        return $this->exchangeRatesBuffer[$input_currency][$output_currency];
    }

    public function fetchExchangeRate($input_currency, $output_currency)
    {
        $currencyIn = new Currency($input_currency);
        $currencyOut = new Currency($output_currency);

        try {
            $rate = $this->container->get('exchange_rate.provider')->fetch($currencyIn, $currencyOut);
            return $rate;
        } catch (CurrencyPairNotSupported $e) {
            return null;
        }
    }

	/**
	 * @param $sentence String to be analysed to understand amounts and currencies to exchange
	 *
	 * @return array Multidimensional array containing conversion data, each having: 'base_amount', 'input_currency', 'output_currency'.
     *          Example:
     *          - For input sentence:
     *              przelicz (“GBP 4000”, “PLN 350”) na (“EUR”, “GBP”)
     *          - The output will lool like this:
     *              array:2 [
     *                  0 => array:3 [
     *                      "base_amount" => 4000
     *                      "input_currency" => "GBP"
     *                      "output_currency" => "EUR"
     *                  ]
     *                  1 => array:3 [
     *                      "base_amount" => 350
     *                      "input_currency" => "PLN"
     *                      "output_currency" => "GBP"
     *                  ]
     *              ]
	 */
    public function parseSentence($sentence)
    {

    	$output = [];

    	/*
    	 * `conversion input string` may looks like:
    	 * - convert GBP 10050 to EUR
    	 * - przelicz (“GBP 4000”, “PLN 350”) na (“EUR”, “GBP”)
    	 * - how much euro is 10050 pounds
    	 */


	    /* Remove unwanted characters from sentence */
	    $sentenceClean = preg_replace("/[^A-Za-z0-9 ]/", "", $sentence);

	    /* Replace known currency names to their codes for a bit easier parsing later */
	    foreach ($this->knownCurrencies as $currencyCode=>$currencyPreferences) {
		    // start with english plural first
	    	if (isset($currencyPreferences['name_plural_en']) && !empty($currencyPreferences['name_plural_en'])) {
			    $sentenceClean = str_ireplace($currencyPreferences['name_plural_en'], $currencyCode, $sentenceClean);
		    }

		    // then try with singular
	    	if (isset($currencyPreferences['name']) && !empty($currencyPreferences['name'])) {
			    $sentenceClean = str_ireplace($currencyPreferences['name'], $currencyCode, $sentenceClean);
		    }
	    }

	    /* Split sentence into words */
	    $words = explode(' ', $sentenceClean);


	    $base_amounts = $input_currency = $output_currency = [];

	    /* Iterate through words to find representation of decimal (or float) number*/
	    for ($i=0; $i<sizeof($words); $i++) {

	    	if ( ! is_numeric($words[$i])) continue;

	        /* It's possibly a base amount to exchange. Now let's check if it's described by any currency. */

	        $previousWord = isset($words[$i - 1]) ? $words[$i - 1] : '';
	        $previousWordAsCode = strtoupper( substr($previousWord,0,3) );

	        $nextWord = isset($words[$i + 1]) ? $words[$i + 1] : '';
	        $nextWordAsCode = strtoupper( substr($nextWord,0,3) );

	        if ( $previousWord && array_key_exists( $previousWordAsCode , $this->knownCurrencies ) ) {
			    /* This is a check if the former word is a currency code, so it looks like "GBP 10050" */

			    $base_amounts[] = intval( $words[$i] );
			    $input_currency[] = $previousWordAsCode;

			    /* We also need to remove these two words as we don't need them anymore */
			    unset($words[$i]);
			    unset($words[$i-1]);

		    } elseif ( $nextWord && array_key_exists( $nextWordAsCode , $this->knownCurrencies )) {
			    /* This is a check if the later word is a currency code, so it looks like "10050 GBP" */

			    $base_amounts[] = intval( $words[$i] );
			    $input_currency[] = $nextWordAsCode;

			    /* We also need to remove these two words as we don't need them anymore */
			    unset($words[$i]);
			    unset($words[$i+1]);

		    }

	    }


	    /* Once again iterate through remaining words to find output currency codes*/
	    foreach ($words as $word) {
	    	$testWord = strtoupper( substr($word, 0, 3) );
	    	if (array_key_exists( $testWord, $this->knownCurrencies) ) {
	    		$output_currency[] = $testWord;
		    }
	    }


	    /*
	     * Now all these three arrays: $base_amounts, $input_currency and $output_currency should have equal number of
	     * items, where:
	     * $base_amounts[$n] is the base amount of currency $input_currency[$n], which needs to be exchanged for
	     * currency $output_currency[$n].
	     */


	    for ($i=0; $i<sizeof($base_amounts); $i++) {
		    $output[] = array(
			    'base_amount'     => ($base_amounts[$i] / 100),
			    'input_currency'  => $input_currency[$i],
			    'output_currency' => $output_currency[$i]
		    );
	    }


	    return $output;
    }

    /**
     * Checks if given currency pair  is allowed by the script
     * @param string $input_currency Input currency ISO code, e.g. GBP
     * @param string $output_currency Output currency ISO code, e.g. PLN
     * @return bool
     */
    public function isCurrencyPairSupported( $input_currency, $output_currency )
    {
        $currencyPair = strtoupper($input_currency.'/'.$output_currency);
        return ( in_array( $currencyPair, $this->allowedCurrencyPairs ));
    }

	public function getKnownCurrencies() {
		return $this->knownCurrencies;
    }
}
