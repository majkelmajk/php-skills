<?php

namespace Application\ExchangeRate;

use Application\Exception\CurrencyPairNotSupported;
use Money\Currency;

class YahooExchangeRate implements ExchangeRateProvider
{
	public function fetch(Currency $currencyIn, Currency $currencyOut)
	{
		$yql = 'select * from yahoo.finance.xchange where pair in ("'. strtoupper($currencyIn->getCode() . $currencyOut->getCode()) .'")';
		$apiRequestUrl = "http://query.yahooapis.com/v1/public/yql?q=". urlencode($yql) ."&env=store://datatables.org/alltableswithkeys";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apiRequestUrl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml')); // Assuming you're requesting JSON
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec($ch);

		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$object = json_decode($json);

		if (is_numeric($object->results->rate->Rate)) {
			return $object->results->rate->Rate;
		}

		throw new CurrencyPairNotSupported();
	}
}
