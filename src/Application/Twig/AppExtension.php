<?php

namespace Application\Twig;

class AppExtension extends \Twig_Extension
{
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('price_part', array($this, 'price_partFilter')),
		);
	}

	public function price_partFilter($number, $part='')
	{
		switch ($part){
			case 'decimals':
				$output = str_pad(($number - floor($number))*100, 2, '0', STR_PAD_LEFT);
				break;
			case 'integer':
				$output = floor($number);
				break;
			case '':
			default:
				$output = number_format($number, 2);
		}
		return $output;
	}
}